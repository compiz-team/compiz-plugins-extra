compiz-plugins-extra (2:0.8.18-5) UNRELEASED; urgency=medium

  * watch: Use api to get tags.

 -- Samuel Thibault <sthibault@debian.org>  Thu, 14 Nov 2024 01:40:47 +0100

compiz-plugins-extra (2:0.8.18-4) unstable; urgency=medium

  [ Samuel Thibault ]
  * patches/implicit-function-declaration,{-2}: Fix build with
    -Werror=implicit-function-declaration (Closes: Bug#1073301)

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 16 Jun 2024 17:08:47 +0200

compiz-plugins-extra (2:0.8.18-3) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Fix using Breaks+Replaces rather than Conflics+Replaces.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on compiz-bcop,
      compiz-plugins-main and libdecoration0-dev.
    + compiz-plugins-extra: Drop versioned constraint on compiz-plugins-main in
      Depends.
    + compiz-plugins-extra: Drop versioned constraint on compiz-dev,
      compiz-plugins and compiz-plugins-default in Replaces.
    + compiz-plugins-extra: Drop versioned constraint on compiz-dev,
      compiz-plugins and compiz-plugins-default in Breaks.
  * Strip unusual field spacing from debian/control.

  [ Samuel Thibault ]
  * control: Remove .git from end of gitlab URL in Homepage field

 -- Samuel Thibault <sthibault@debian.org>  Sat, 17 Sep 2022 21:32:50 +0200

compiz-plugins-extra (2:0.8.18-2) unstable; urgency=medium

  [ Samuel Thibault ]
  * Update upstream URL
  * control: Bump Standards-Version to 4.6.0 (no change)
  * watch: Update URL.
  * control: Set Rules-Requires-Root to no.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Drop unnecessary dependency on dh-autoreconf.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.5.1, no changes needed.
  * Add patch ac-path-pkgconfig.patch: Use cross-build compatible macro for
    finding pkg-config.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 08 Jan 2022 17:49:45 +0100

compiz-plugins-extra (2:0.8.18-1) unstable; urgency=medium

  * New upstream release.
    - control: Replace old version of compiz-plugins-experimental due to
    migration of workspacenames plugin.
    - control: Build-dep on libcairo2-dev libcanberra-dev libxrender-dev
    libxsettings-client-dev.
    - control: Bump compiz dependency to 0.8.18 to get latest
    compiz-core-abiversion.
  * watch: Generalize pattern.
  * control: Remove Cyril from uploaders.
  * watch: switch to gitlab

 -- Samuel Thibault <sthibault@debian.org>  Mon, 06 Apr 2020 01:28:29 +0200

compiz-plugins-extra (2:0.8.16-2) unstable; urgency=medium

  * Upload to unstable.
  * Move .pc files to a multiarch location.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 15 Dec 2018 02:36:49 +0100

compiz-plugins-extra (2:0.8.16-1) experimental; urgency=medium

  * New upstream release.
    - control: Bump compiz dependency.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 17 Nov 2018 13:47:31 +0100

compiz-plugins-extra (2:0.8.14-1) unstable; urgency=medium

  * Split back packaging according to compiz-reloaded new upstream, and bump
    epoch accordingly
  * Switch to hypra maintenance team
  * Restore non-reloaded package names
  * Keep compatibility with packages in Debian
  * Add libpango1.0-dev build-dep.
  * Update copyright file
  * Generate dbgsym packages

 -- Samuel Thibault <sthibault@debian.org>  Sat, 27 Oct 2018 21:11:54 +0200

compiz-plugins-extra (1:0.9.13.1+18.04.20180302-2) unstable; urgency=medium

  * Last compiz-from-launchpad upload.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 20 Oct 2018 15:14:00 +0200

compiz-plugins-extra (1:0.8.4-4ubuntu1) maverick; urgency=low

  * Bumped epoch

 -- Robert Ancell <robert.ancell@canonical.com>  Thu, 17 Jun 2010 10:12:18 +1000

compiz-plugins-extra (0.8.14-0~stretch1) stretch; urgency=low

  * New upstream release.
    - rename source package to reloaded version
  * Add override dh_strip to no create automatic dbgsym.
  * Remove Gtk2 requirement, not needed.
  * Bump standards to version 3.9.8.

 -- Jof Thibaut <compiz@tuxfamily.org>  Sun, 25 Jun 2017 14:29:14 +0200

compiz-fusion-plugins-extra (0.8.12-0~jessie1) jessie; urgency=low

  * New upstream release.
    - forked on Github : Compiz Reloaded
  * Switch to format 3.0 (quilt).
  * Update compat and rules for debhelper 9.
  * Bump standards to version 3.9.6.
  * Change Priority to optional.
  * Update copyright to format 1.0 and Homepage link.
  * Drop libgconf-dev and add libgtk2.0-dev on build-depends.

 -- Jof Thibaut <compiz@tuxfamily.org>  Thu, 03 Mar 2016 00:02:17 +0100

compiz-fusion-plugins-extra (0.8.4-2) unstable; urgency=low

  * Add Vcs-* fields.
  * Tighten dependencies on compiz-core:
     - Add ${compizcore:Depends} to Depends.
     - Use dpkg-query to extract the name of the package provided by
       compiz-core (e.g. compiz-core-abiversion-20090619), and use it in
       dh_gencontrol.

 -- Cyril Brulebois <kibi@debian.org>  Wed, 24 Mar 2010 00:45:05 +0100

compiz-fusion-plugins-extra (0.8.4-1) unstable; urgency=low

  * New upstream release.
  * Update Build-Depends against compiz-dev to >= 0.8.4 for ABI bump.

 -- Sean Finney <seanius@debian.org>  Thu, 11 Feb 2010 21:58:40 +0100

compiz-fusion-plugins-extra (0.8.2-3) unstable; urgency=low

  * bump build-deps again; there was an unintended (and reverted) ABI
    break.

 -- Sean Finney <seanius@debian.org>  Sat, 11 Apr 2009 19:38:15 +0200

compiz-fusion-plugins-extra (0.8.2-2) unstable; urgency=low

  * update versioned build-deps for compiz packages to >= 0.8.2
    (Closes: #522989)

 -- Sean Finney <seanius@debian.org>  Sat, 11 Apr 2009 16:05:06 +0200

compiz-fusion-plugins-extra (0.8.2-1) unstable; urgency=low

  * New upstream release.
  * Add gbp.conf for use with git-buildpackage.
  * Add build-dep on libglu1-mesa-dev

 -- Sean Finney <seanius@debian.org>  Tue, 07 Apr 2009 08:56:36 +0200

compiz-fusion-plugins-extra (0.7.6-1) unstable; urgency=low

  * New upstream release.

 -- Sean Finney <seanius@debian.org>  Tue, 03 Jun 2008 00:57:11 +0200

compiz-fusion-plugins-extra (0.7.4-1) unstable; urgency=low

  * New upstream release.
  * Update build-deps on bcop, compiz-dev, and compiz-fusion-plugins-main
    to >= 0.7.3.

 -- Sean Finney <seanius@debian.org>  Mon, 19 May 2008 20:10:44 +0200

compiz-fusion-plugins-extra (0.6.1-1) unstable; urgency=low

  * New upstream release on the 0.6.0 branch.

  [Emilio Scalise]
  * Added missing build deps: librsvg2-dev, libcairo2-dev, libsm-dev,
    libdbus-1-dev, libjpeg62-dev, libdecoration0-dev (closes: #458159).

 -- Sean Finney <seanius@debian.org>  Mon, 31 Dec 2007 20:03:44 +0100

compiz-fusion-plugins-extra (0.6.0-3) unstable; urgency=low

  * Update copyright information to be more specific, at the request of
    ftp-master.

 -- Sean Finney <seanius@debian.org>  Sun, 25 Nov 2007 18:26:35 +0100

compiz-fusion-plugins-extra (0.6.0-2) unstable; urgency=low

  * Remove autofoo generated content and create/remove it as part of the
    standard build process instead.
  * FTBFS fixes:  add Build-Deps on automake1.9, compiz-fusion-bcop,
    compiz-fusion-plugins-main (yes, this is needed), libgconf2-dev,
    libglib2.0-dev, libtool, libxml2-dev, libxslt1-dev.

 -- Sean Finney <seanius@debian.org>  Sat, 24 Nov 2007 13:10:17 +0100

compiz-fusion-plugins-extra (0.6.0-1) unstable; urgency=low

  * Initial release (Closes: #431749).

 -- Sean Finney <seanius@debian.org>  Sat, 10 Nov 2007 10:20:56 +0100
